/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 2, 2016 8:31:49 PM                      ---
 * ----------------------------------------------------------------
 */
package com.shaneco.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedShanecoFacadesConstants
{
	public static final String EXTENSIONNAME = "shanecofacades";
	
	protected GeneratedShanecoFacadesConstants()
	{
		// private constructor
	}
	
	
}
