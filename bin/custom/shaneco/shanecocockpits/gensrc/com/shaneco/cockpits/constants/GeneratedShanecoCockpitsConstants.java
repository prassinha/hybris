/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 2, 2016 8:31:49 PM                      ---
 * ----------------------------------------------------------------
 */
package com.shaneco.cockpits.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedShanecoCockpitsConstants
{
	public static final String EXTENSIONNAME = "shanecocockpits";
	
	protected GeneratedShanecoCockpitsConstants()
	{
		// private constructor
	}
	
	
}
