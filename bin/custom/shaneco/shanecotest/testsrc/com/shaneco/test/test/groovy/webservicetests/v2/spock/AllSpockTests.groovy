package com.shaneco.test.test.groovy.webservicetests.v2.spock

import de.hybris.bootstrap.annotations.IntegrationTest
import com.shaneco.test.test.groovy.webservicetests.TestSetupUtils
import com.shaneco.test.test.groovy.webservicetests.v2.spock.access.AccessRightsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.access.OAuth2Test
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartDeliveryTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartEntriesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartMergeTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartPromotionsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartResourceTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.CartVouchersTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.GuestsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.OrderPlacementTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.SavedCartFullScenarioTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.carts.SavedCartTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.catalogs.CatalogsResourceTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.customergroups.CustomerGroupsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.errors.ErrorTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.export.ExportTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.filters.CartMatchingFilterTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.filters.UserMatchingFilterTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.flows.AddressBookFlow
import com.shaneco.test.test.groovy.webservicetests.v2.spock.flows.CartFlowTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.general.StateTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.CardTypesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.CurrenciesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.DeliveryCountriesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.LanguagesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.LocalizationRequestTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.misc.TitlesTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.orders.OrdersTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.products.ProductResourceTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.products.ProductsStockTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.promotions.PromotionsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.stores.StoresTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.users.UserAccountTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.users.UserAddressTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.users.UserOrdersTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.users.UserPaymentsTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.users.UsersResourceTest
import com.shaneco.test.test.groovy.webservicetests.v2.spock.general.HeaderTests

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.slf4j.LoggerFactory


@RunWith(Suite.class)
@Suite.SuiteClasses([
	AccessRightsTest, OAuth2Test, StateTest, CartDeliveryTest, CartMergeTest, CartEntriesTest, CartPromotionsTest,
	CartResourceTest, CartVouchersTest, GuestsTest, OrderPlacementTest, CatalogsResourceTest, CustomerGroupsTest, ErrorTest, ExportTest,
	AddressBookFlow, CartFlowTest, CardTypesTest, CurrenciesTest, DeliveryCountriesTest, LanguagesTest, LocalizationRequestTest, TitlesTest,
	OrdersTest, ProductResourceTest, ProductsStockTest, PromotionsTest, SavedCartTest ,SavedCartFullScenarioTest, StoresTest, UserAccountTest,
	UserAddressTest, UserOrdersTest, UserPaymentsTest, UsersResourceTest, CartMatchingFilterTest, UserMatchingFilterTest, HeaderTests])
@IntegrationTest
class AllSpockTests {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AllSpockTests.class)

	@BeforeClass
	public static void setUpClass() {
		TestSetupUtils.loadData()
		TestSetupUtils.startServer()
	}

	@AfterClass
	public static void tearDown(){
		TestSetupUtils.stopServer();
		TestSetupUtils.cleanData();
	}

	@Test
	public static void testing() {
		//dummy test class
	}
}
